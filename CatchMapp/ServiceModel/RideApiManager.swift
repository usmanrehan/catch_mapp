//
//  RideApiManager.swift
//  FastCab User
//
//  Created by Hassan Khan on 8/2/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON

class RideApiManager: APIManagerBase {
    //MARK: - PromoCode
    func submitPromoCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.submitPromoCode)!
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - estimatedRide
    func estimatedFareWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.estimatedFare)!
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /createRide
    func createRideWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.createRide)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /getDrivers
    func getNearByDriversWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.getNearbyDrivers)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - GET /ride/approveridedetail
    func getRideDetailWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.approveRideDetail)!
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /changeRideStatus
    func changeRideStatus(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.rideStatus)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /getLocationHistory
    func getLocationHistoryWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.locationHistory)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /ride/userinprogressride
    func getUserRideUpcomingWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.upcomingRide)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /ride/usercompleteride
    func getUserRidePastWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.pastRide)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
}
