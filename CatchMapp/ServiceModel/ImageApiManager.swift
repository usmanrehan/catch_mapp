//
//  ImageApiManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/25/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage

class ImageApiManager: APIManagerBase {
    //MARK: - /ImageDownloader
    func downloadImage(url: String, success: @escaping DefaultImageResultClosure)  {
        self.downloadImageT(route: url, success: success)
    }
}
