//
//  CMSApiManager.swift
//  FastCab User
//
//  Created by Hassan Khan on 8/2/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CMSApiManager: APIManagerBase {
    //MARK:- /getVehicleCat
    func getvehicleCategoriesWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.getVehicleCat)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /contactus
    func contactWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.contactUs)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /getCms getCms
    func getCMSWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.getCms)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /getMessages
    func getMessagesWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.driverMessages)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /getMessages
    func getCancelReasonWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.cancelReasons)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /userFeedback
    func submitUserFeedback(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.userFeedback)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /getImproveType
    func getImproveTypeWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.improveType)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: -/ /cms/appfeedback
    func submitAppFeedbackWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.appFeedback)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
