//
//  FavouriteApiManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/27/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class FavouriteApiManager: APIManagerBase {
    //MARK: - /addFavourites
    func addFavouritesWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders:headers)
        
    }
    //MARK: - /removeFavourite 
    func removeFavourite(params: [String:String], serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool) {
        
        var route = ""
        route = baseURL + serviceName
        for (_,value) in params {
            route = route + "/\(value)"
        }
        self.deleteRequestWithT(endPoint: route, success: success, failure: failure)
    }
    //MARK: - /getFavourite
    func getFavouritesWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
}
