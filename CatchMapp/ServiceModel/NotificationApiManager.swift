//
//  NotificationApiManager.swift
//  FastCab User
//
//  Created by Hassan Khan on 8/1/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationApiManager: APIManagerBase {
    //MARK: - /login
    func changePushStatusWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.changeStatus)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- /getNotifications
    func getNotificationsWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        let route: URL = GETURLfor(route: Constants.getNotifications)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
}
