//
//  SellerApiManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/24/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SellerApiManager: APIManagerBase {
    
    //MARK: - DINING
    //MARK: - /getSellers
    func getSellersWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = GETURLfor(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /dinningCategories
    func getDinningCategoriesWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
    
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /adminPromotions
    func getAdminPromotionsWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /getDeals
    func getDealsWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /tableBooking
    func bookTableWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders:headers)
        
    }
    
    //MARK: - SHOPING
    //MARK: - /getSubCategories
    func getSubCategoriesWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /getSubCategoriesData
    func getSubCategoriesDataWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
        //self.getRequestArrayWithTemp(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: -  /getIndividualSeller
    func getIndividualSellerWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
        
    }
    
    //MARK:- CMS
    //MARK: - //getcms
    func getCMSDataWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    
    //MARK: -  GetAllDeals
    //MARK: - /getAllDeals
    func getAllDealsWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
}
