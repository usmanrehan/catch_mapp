//
//  GuestUsersAPIManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/20/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class GuestUsersAPIManager: APIManagerBase {
    //MARK: - /getUserToken
    func loginWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool)  {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: headers)
    }
}
