//
//  UsersAPIManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/20/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UsersAPIManager: APIManagerBase {
    //MARK: - /login
    func loginWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.login)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /sendCode
    func sendCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.sendCode)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /verifyCode
    func verifyCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.verifyCode)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /ResetCode
    func resetCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.resetCode)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /resetPassword
    func resetPasswordWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.forgotPassword)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /register
    func registerWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.register)!
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /forgotpassword
    func forgotPasswordWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool) {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: headers)
    }
    //MARK: - /facebooklogin
    func facebookLoginWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool ) {
        
        let route: URL = POSTURLforRoute(route: serviceName)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: headers)
    }
    //MARK: - /logout
    func logOutWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.logout)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK:- /getProfile
    func getProfileWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = GETURLfor(route: Constants.getProfile)!
        
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /update
    func updateProfileWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.updateProfile)!
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure)
    }
    //MARK: - /changePAssword
    func changePasswordWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.changePassword)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    //MARK: - /updateDeviceToken
    func updateDeviceTokenWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.updateDeviceToken)!
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
}
