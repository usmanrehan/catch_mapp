//
//  ReviewsApiManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/26/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReviewsApiManager: APIManagerBase {
    //MARK: - /getReviews
    func getReviewsWith(params: Parameters, serviceName: String, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure)  {
        
        let route: URL = GETURLfor(route: serviceName)!
        
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure)
    }
    
    //MARK: - /addReview
    func addReviewWith(params: Parameters, serviceName: String, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure, headers: Bool)  {
        
        let route: URL = GETURLfor(route: serviceName)!
        
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders:headers)
    }
}

